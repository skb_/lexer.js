# Lexer.js

A lexer generator based on recursive-descent. You can use it to generate a lexer based on a list of rules that you pass in, pass it your own matching functions, or you can have a mixture of both. The order of this list of matching rules determines precedence.